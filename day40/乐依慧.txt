﻿HTML初识
	•超文本标记语言（Hypertext Markup Language, HTML）是一种用于创建网页的标记语言。


HTML基本文本结构：
	<!DOCTYPE html>         #必须要有，且在开头
	<html lang="zh-CN">   

	<head> 
	  <meta charset="UTF-8">
	  <title>标题</title>    #网页标题
	</head>
	<body> 
	                         #网页可见的主题内容
	</body>
	</html>


标签的语法：
	•<标签名 属性1=“属性值1” 属性2=“属性值2”……>内容部分</标签名> 
	•<标签名 属性1=“属性值1” 属性2=“属性值2”…… />


注释：
	<!--被注释内容-->
	ctrl+/


body中常用的标签：**
（只要是用户可以操作的，均有disable属性）
（凡是选择框，都加value="“
段落标签（独占一个段落）:   <p>我们一起走，然后各自幸福.脚踏实地谋发展，努力努力再努力</p>

六级标题：      <h1>标题</h1>
				<h2>标题</h2>
				<h3>标题</h3>
				<h4>标题</h4>
				<h5>标题</h5>
				<h6>标题</h6>

换行：   <br>/<hr>

div VS span:
div标签用来定义一个块级元素，并无实际的意义。主要通过CSS样式为其赋予不同的表现。
span标签用来定义内联元素，并无实际的意义。主要通过CSS样式为其赋予不同的表现.
#标签的嵌套：块级标签可以包含内联标签，而内敛标签不可以包含块级标签（p标签例外，即使p标签包含了p标签，系统也会自动补全）
#块级标签：独占一行
#内敛标签：不独占一行

a标签（超链接标签）：
语法--><a href="要跳转到的位置或网址" target="_blank">要显示在点击处的提示语</a>
eg.<a href="http://www.baidu.com" target="_blank">点击此处</a>

列表：
无序列表：
语法--><ul type="disc(实心点)/circle(空心点)/square(实心方块)/none(无)">
		   <li>内容1</li>
		   <li>内容2</li>
		</ul>
eg.<ul type="disc">
    <li>“我们一起走，然后各自幸福
        脚踏实地谋发展，努力努力再努力”</li>
    <li>“情深难几许，聚散多两依。
        今朝此为别，他处还相遇。”</li>
	</ul>
有序列表：
语法--><ol type="1(数字 默认)/A(大写字母)/a(小写字母)/I(大写罗马数字)/i(小写罗马数字) start="从数字几开始的数字">
		   <li>内容1<li>
		   <li>内容2<li>
		</ol>  
eg.</ol><ol type="1" start="2">
    <li>“我们一起走，然后各自幸福
        脚踏实地谋发展，努力努力再努力”</li>
    <li>“情深难几许，聚散多两依。
        今朝此为别，他处还相遇。”</li>
	</ol>
	
层级的标题列表：
语法--><dl>
		 <dt>标题1</dt>
		 <dd>内容1</dd>
		 <dt>标题2</dt>
		 <dd>内容1</dd>
		<dl>
eg.</dl>>
    <dt>lay</dt>
    <dd>“我们一起走，然后各自幸福
        脚踏实地谋发展，努力努力再努力”</dd>
    <dt>xingshaolin</dt>
    <dd>“情深难几许，聚散多两依。
        今朝“此为别，他处还相遇。”</dd>
	</dl>
	
表格：
语法--><table border="1">
		<thead>
		<tr>
			<th>序号</th>
			<th>姓名</th>
			<th>爱好</th>
		</tr>
		<thead>
		<tbody>
		<tr>
			<td>1</td>
			<td>Egon</td>
			<td>负责老师</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Yuan</td>
			<td>日天</td>
		</tr>
		</tbody>
	</table>
	

input标签：
单行文本：<input type="text"/>
密码输入框：<input type="password"/>
日期输入框：<input type="date"/>
复选框：<input type="chekbox"checked="checked"name="x"/>
单选框：<input type="radio" name="x"/>
提交按钮：<input type="submit"value="提交"/>
普通按钮：<input type="button"value="普通按钮"/>
隐藏输入框：<input type="hidden"/>

select标签：（id值唯一，不可重）

labal标签：

textarea标签：


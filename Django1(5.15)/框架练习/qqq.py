# import socket
# sk = socket.socket()
# sk.bind(('127.0.0.1',9099))
# sk.listen()
# while 1:
#     conn, addr = sk.accept()
#     client_msg = conn.recv(1024).decode("utf-8")
#     # print('浏览器请求消息',client_msg)
#     conn.send(b'HTTP/1.1 200 ok \r\n\r\n')
#     # print(client_msg)
#     path = client_msg.split("\r\n")[0].split(" ")[1]
#     # print(path)
#     path_list = ["/cute.jpg","/test.css"]
#     if path == "/":
#         with open('test.html','rb') as f:
#             f_data=f.read()
#         conn.send(f_data)
#     elif path in path_list:
#         with open('{}'.format(path[1::]),'rb') as f:
#             c_data=f.read()
#         conn.send(c_data)





import socket
from threading import Thread
sk = socket.socket()
sk.bind(('127.0.0.1', 9099))
sk.listen()
def func(conn):
    client_msg = conn.recv(1024).decode('utf-8')
    print(client_msg)
    path = client_msg.split('\r\n')[0].split(' ')[1]
    if path == '/':
        conn.send(b'HTTP/1.1 200 ok \r\n\r\n')
        with open('test.html', 'rb') as f:
            f_data = f.read()
        conn.send(f_data)
    elif path == '/test.css':
        conn.send(b'HTTP/1.1 200 ok \r\n\r\n')
        with open('test.css', 'rb') as f:
            c_data = f.read()
        conn.send(c_data)
    elif path == '/test.js':
        conn.send(b'HTTP/1.1 200 ok \r\n\r\n')
        with open('test.js', 'rb') as f:
            j_data = f.read()
        conn.send(j_data)
    elif path == '/cute.jpg':
        conn.send(b'HTTP/1.1 200 ok \r\n\r\n')
        with open('cute.jpg', 'rb')as f:
            x_data = f.read()
        conn.send(x_data)
    elif path=='/fox.ico':
        conn.send(b'HTTP/1.1 200 ok \r\n\r\n')
        with open('fox.ico','rb') as f:
            o_data=f.read()
        conn.send(o_data)
while 1:
    conn, addr = sk.accept()
    t = Thread(target=func, args=(conn,))
    t.start()
